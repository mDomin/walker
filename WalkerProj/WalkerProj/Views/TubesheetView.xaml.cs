﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WalkerProj.ViewModels;
using System.Data;

namespace WalkerProj.Views
{
  /// <summary>
  /// Interaction logic for TubesheetView.xaml
  /// </summary>
  public partial class TubesheetView : Window
  {
    public TubesheetView()
    {
      InitializeComponent();
      DataContext = new TubesheetViewModel("../../Tubesheet.txt");

    }

    //public void InitializeComponent()
    //{
    //  DataContext = new TubesheetViewModel("../XMLFolder/");
    //}
  }
}

