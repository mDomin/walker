﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalkerProj.Models
{
  public class Tube
  {
    private TubeStatus _status;
    private readonly int _row;
    private readonly int _column;

    public Tube(TubeStatus s, int row, int column)
    {
      Status = s.ToString();
      _row = row;
      _column = column;
    }

    public string Status
    {
      get { return _status.ToString(); }
      set { _status = (TubeStatus)Enum.Parse(typeof(TubeStatus), value); }
    }

    /// <summary>
    /// Return bottom position of tube
    /// </summary>
    public int Bottom
    {
      get { return _column*10; }
    }

    /// <summary>
    /// Return left position fo tube
    /// </summary>
    public int Left
    {
      get { return _row*10; }
    }

    public int Row
    {
      get { return _row; }
    }

    public int Column
    {
      get { return _column; }
    }

    public string Color
    {
      get { return GetString(_status); }
    }

    public static string GetString(TubeStatus stat)
    {
      switch (stat)
      {
        case TubeStatus.Critical:
          return "Red";
        case TubeStatus.Plugged:
          return "Black";
        case TubeStatus.Unknown:
          return "Gainsboro";
        default:
          return "White";
      }
    }
    
  }
  public enum TubeStatus
  {
    Unknown,
    Plugged,
    Critical
  }
}
