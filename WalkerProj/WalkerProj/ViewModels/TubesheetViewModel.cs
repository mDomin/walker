﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WalkerProj.Models;

namespace WalkerProj.ViewModels
{
  public class TubesheetViewModel
  {
    private List<Tube> _tubesheet = new List<Tube>();
    private string _xmlFilePath;

    private int _tubeRadius = 8;

    //private AddTube()

    //private int = 

    private void ParseXML(string xmlFilePath)
    {
      _xmlFilePath = xmlFilePath;
      XmlDocument xmlDoc = new XmlDocument();
      xmlDoc.Load(xmlFilePath);

      float diameter = float.Parse(xmlDoc.DocumentElement.SelectSingleNode("/TubesheetModel/TubesheetDiameter").InnerText);
      float pitch = float.Parse(xmlDoc.DocumentElement.SelectSingleNode("/TubesheetModel/TubesheetPitch").InnerText);

      XmlNodeList xmlTubeList = xmlDoc.DocumentElement.SelectNodes("/TubesheetModel/Tubes/Tube");

      foreach (XmlNode node in xmlTubeList)
      {
        string status = node.SelectSingleNode("Status").InnerText;        

        int row = Int32.Parse(node.SelectSingleNode("Row").InnerText);
        int column = Int32.Parse(node.SelectSingleNode("Column").InnerText);

        Tube t = new Tube((TubeStatus)Enum.Parse(typeof(TubeStatus), status), row, column);

        Tubesheet.Add(t);

        //_tubesheet[row][column] = t;
      }
    }

    public TubesheetViewModel(string xmlFilePath)
    {
      //Tubesheet.Add(new Tube(TubeStatus.Plugged, 100, 100) );
      //Tubesheet.Add( new Tube(TubeStatus.Critical, 50, 100) );
      ParseXML(xmlFilePath);
    }

    public List<Tube> Tubesheet
    {
      get { return _tubesheet; }
    }

    public int TubeRadius
    {
      get { return _tubeRadius; }
    }
     
  }
}
